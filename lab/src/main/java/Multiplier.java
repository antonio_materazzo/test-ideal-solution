public class Multiplier {
    private int x;
    private int y;

    public Multiplier(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int mul(){
        return x*y;
    }

    public int pow(int p){
        int ret = x;
        for(int i=1; i<p; i++)
            ret*=x;
        return ret;
    }

}
