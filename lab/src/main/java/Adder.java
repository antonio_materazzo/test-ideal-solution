public class Adder {
    private int x;
    private int y;

    public Adder(int x, int y){
        this.x = x;
        this.y = y;
    }

    //somma x e y
    public int add(){
        return x+y;
    }

    //sottrae x e y
    public int sub(){
        return x-y;
    }

}
