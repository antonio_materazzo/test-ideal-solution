import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

import static org.junit.Assert.assertTrue;

public class FormalityTest {

    private int testMaxNumber = 4;

    @Test
    public void testsAndAssertionsTest() throws IOException {
        String rawContent = Files.readString(Path.of("src/test/java/TestClass.java"));
        String content = removeComments(rawContent);
        System.out.println(content);
        Scanner sc = new Scanner(content);
        int testCounter = 0;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if(line.contains("@Test") ){
                testCounter++;
                if(testCounter > testMaxNumber) break;
                sc.nextLine(); //mi posiziono sulla firma del metodo
                assertTrue(methodHasOneAssert(sc)); //verifica che il test abbia al suo interno al più un assert
            }
        }
        assertTrue(testCounter <= testMaxNumber); //verifica che non ci siano più del numero massimo di test
    }

    private static boolean methodHasOneAssert(Scanner sc){
        int assertionNumber = parseMethod(sc);
        return assertionNumber <= 1;
    }

    //ritorna il numero di assert nel metodo
    //lo scanner è posizonato sulla firma del metodo (next porta alla prima riga del body)
    private static int parseMethod(Scanner sc){
        int assertionNumber = 0;
        int braceCount = 1; //contatore delle parentesi graffe per capire quando finisce il metodo
        while(braceCount != 0){
            String line = sc.nextLine();
            //se line è un for o while non può avere assert al suo interno: se ne trovo li metto a Integer.MAX_VALUE
            if(line.matches("([ \n\t{};]+)*(for|while) *\\(.*")){
                //invoco parseMethod per parsare il blocco for/while
                if(parseMethod(sc)>0) {
                    assertionNumber = Integer.MAX_VALUE;
                    break;
                }
            }
            else {
                //controllo il numero di assert e parentesi
                assertionNumber += StringUtils.countMatches(line, "assert");
                braceCount += StringUtils.countMatches(line, "{");
                braceCount -= StringUtils.countMatches(line, "}");
            }
        }
        return assertionNumber;
    }

    private static String removeComments(String s){
        String partialFiltered = s.replaceAll("/\\*[\\s\\S]*?\\*/", "");
        String fullFiltered = partialFiltered.replaceAll("//.*", "");
        return fullFiltered;
    }

}
